<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('components.frontend.partials.content');
});

Route::get('/cart', function () {
    return view('components.frontend.common.cart');
});

Route::get('/checkout', function () {
    return view('components.frontend.common.checkout');
});

Route::get('/order', function () {
    return view('components.frontend.common.order');
});

Route::get('/product_simple', function () {
    return view('components.frontend.common.product-simple');
});

Route::get('/categories', function () {
    return view('components.frontend.common.shop-classic-filter');
});


Route::get('/admin', function () {
    return view('components.backend.partials.contents');
});
Route::get('/error', function () {
    return view('components.backend.common.404');
});
Route::get('/blank', function () {
    return view('components.backend.common.blank');
});
Route::get('/button', function () {
    return view('components.backend.common.button');
});
Route::get('/chart', function () {
    return view('components.backend.common.chart');
});
Route::get('/delete', function () {
    return view('components.backend.common.delete');
});
Route::get('/edit', function () {
    return view('components.backend.common.edit');
});
Route::get('/element', function () {
    return view('components.backend.common.element');
});
Route::get('/form', function () {
    return view('components.backend.common.form');
});
Route::get('/signin', function () {
    return view('components.backend.common.signin');
});
Route::get('/signup', function () {
    return view('components.backend.common.signup');
});
Route::get('/table', function () {
    return view('components.backend.common.table');
});
Route::get('/typography', function () {
    return view('components.backend.common.typography');
});
Route::get('/widget', function () {
    return view('components.backend.common.widget');
});
