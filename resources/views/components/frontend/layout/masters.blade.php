<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Riode - Ultimate eCommerce Template</title>

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Riode - Ultimate eCommerce Template">
    <meta name="author" content="D-THEMES">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ asset ('ui/frontend/images/icons/favicon.png')}}">
    <!-- Preload Font -->
    <link rel="preload" href="{{ asset ('ui/frontend/fonts/riode.ttf?')}}" as="font" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" href="{{ asset ('ui/frontend/vendor/fontawesome-free/webfonts/fa-solid-900.woff2')}}" as="font" type="font/woff2"
        crossorigin="anonymous">
    <link rel="preload" href="{{ asset ('ui/frontend/vendor/fontawesome-free/webfonts/fa-brands-400.woff2')}}" as="font" type="font/woff2"
        crossorigin="anonymous">
    <script>
        WebFontConfig = {
            google: { families: [ 'Poppins:300,400,500,600,700,800' ] }
        };
        ( function ( d ) {
            var wf = d.createElement( 'script' ), s = d.scripts[ 0 ];
            wf.src = "{{ asset ('ui/frontend/js/webfont.js')}}";
            wf.async = true;
            s.parentNode.insertBefore( wf, s );
        } )( document );
    </script>


    <link rel="stylesheet" type="text/css" href="{{ asset ('ui/frontend/vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('ui/frontend/vendor/animate/animate.min.css')}}">

    <!-- Plugins CSS File -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('ui/frontend/vendor/magnific-popup/magnific-popup.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('ui/frontend/vendor/owl-carousel/owl.carousel.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{ asset ('ui/frontend/vendor/sticky-icon/stickyicon.css')}}">

    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('ui/frontend/css/demo2.min.css')}}">
</head>

<body class="home">

    <div class="page-wrapper">
        <h1 class="d-none">Riode - Responsive eCommerce HTML Template</h1>
<x-frontend.partials.header/>
        <!-- End Header -->
           {{ $slot }}
        <!-- End of Main -->

        <x-frontend.partials.footer/>
	<!-- Plugins JS File -->
    <script src="{{ asset ('ui/frontend/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset ('ui/frontend/vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{ asset ('ui/frontend/vendor/elevatezoom/jquery.elevatezoom.min.js')}}"></script>
    <script src="{{ asset ('ui/frontend/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset ('ui/frontend/vendor/isotope/isotope.pkgd.min.js')}}"></script>
    <script src="{{ asset ('ui/frontend/vendor/skrollr/skrollr.min.js')}}"></script>
    <script src="{{ asset ('ui/frontend/vendor/owl-carousel/owl.carousel.min.')}}"></script>
    <!-- Main JS File -->
    <script src="{{ asset ('ui/frontend/js/main.min.js')}}"></script>
</body>

</html>