<nav class="navbar bg-secondary navbar-dark">
                <a href="/admin" class="navbar-brand mx-4 mb-3">
                    <h3 class="text-primary"><i class="fa fa-user-edit me-2"></i>DarkPan</h3>
                </a>
                <div class="d-flex align-items-center ms-4 mb-4">
                    <div class="position-relative">
                        <img class="rounded-circle" src="ui/backend/img/user.jpg" alt="" style="width: 40px; height: 40px;">
                        <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
                    </div>
                    <div class="ms-3">
                        <h6 class="mb-0">Fouzia Nawrin </h6>
                        <span>Admin</span>
                    </div>
                </div>
                <div class="navbar-nav w-100">
                    <a href="/admin" class="nav-item nav-link active"><i class="fa fa-tachometer-alt me-2"></i>Dashboard</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><i class="fa fa-laptop me-2"></i>Products</a>
                        <div class="dropdown-menu bg-transparent border-0">
                            <a href="/table" class="dropdown-item">Show Product</a>
                            <a href="/button" class="dropdown-item">Add Product</a>
                            <a href="/typography" class="dropdown-item">Update Product</a>
                            <a href="/delete" class="dropdown-item">Delete Product</a>
                        </div>
                    </div>
                    <a href="/widget" class="nav-item nav-link"><i class="fa fa-th me-2"></i>Widgets</a>
                    <a href="/form" class="nav-item nav-link"><i class="fa fa-keyboard me-2"></i>Forms</a>
                    <a href="/table" class="nav-item nav-link"><i class="fa fa-table me-2"></i>Tables</a>
                    <a href="/chart" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Charts</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><i class="far fa-file-alt me-2"></i>Pages</a>
                        <div class="dropdown-menu bg-transparent border-0">
                            <a href="/signin" class="dropdown-item">Sign In</a>
                            <a href="/signup" class="dropdown-item">Sign Up</a>
                            <a href="/home" class="dropdown-item">Main Page</a>
                        </div>
                    </div>
                </div>
            </nav>
        </div>